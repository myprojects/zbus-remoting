package org.zbus.remoting;

import org.zbus.remoting.pool.RemotingClientPool;
import org.zbus.remoting.pool.RemotingClientPoolConfig;

public class RemotingClientPoolTest {

	public static void main(String[] args) throws Exception {
		for(int i=0;i<1;i++){
			test();
		}
	}

	public static void test() throws Exception {  
		
		RemotingClientPoolConfig config = new RemotingClientPoolConfig(); 
		config.setBrokerAddress("127.0.0.1:80"); 
		RemotingClientPool pool = new RemotingClientPool(config);
		 
		RemotingClient client = pool.borrowObject();
		Message msg = new Message();
		msg.setCommand("hello");
		Message res = client.invokeSync(msg);
		System.out.println(res);
		pool.returnObject(client); 
	
		
		pool.close(); 
		
		System.out.println("done");
	}

}

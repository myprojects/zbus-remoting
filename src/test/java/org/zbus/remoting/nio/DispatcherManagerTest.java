package org.zbus.remoting.nio;

import org.zbus.remoting.MessageCodec;

public class DispatcherManagerTest {

	public static void main(String[] args) throws Exception {
		DispatcherManager mgr = new DispatcherManager(new MessageCodec()) { 
			@Override
			public EventAdaptor buildEventAdaptor() { 
				return null;
			}
		};
		
		mgr.start();
		mgr.close();
	} 
}

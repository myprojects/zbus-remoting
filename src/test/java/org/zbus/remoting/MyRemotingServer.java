package org.zbus.remoting;

import java.io.IOException;

import org.zbus.remoting.nio.Session;

public class MyRemotingServer extends RemotingServer {
	
	//个性化定义Message中的那部分解释为命令，这里为了支持浏览器直接访问，把Message中的path理解为command
	@Override
	public String findHandlerKey(Message msg) { 
		String cmd = msg.getCommand();
		if(cmd == null){
			cmd = msg.getPath();
		}
		return cmd;
	}
	
	public MyRemotingServer(int serverPort) throws IOException {
		super(serverPort); 
		//注册命令处理Callback
		this.registerHandler("hello", new MessageHandler() {
			public void handleMessage(Message msg, Session sess) throws IOException {
				System.out.println(msg);
				msg.setStatus("200");   
				msg.setBody("hello world");
				sess.write(msg);
			}
		});
	}

	public static void main(String[] args) throws Exception {   
		@SuppressWarnings("resource")
		MyRemotingServer server = new MyRemotingServer(80);
		server.start(); 
	}
}
